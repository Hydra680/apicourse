"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.profile = exports.login = exports.registraEstudiante = void 0;
const database_1 = require("../database");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const bcryptPassword_1 = require("../Function/bcryptPassword");
exports.registraEstudiante = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { ci, nombre, ap_paterno, ap_materno, correo, fono, institucion, fecha_nacimiento } = req.body;
    const password = yield bcryptPassword_1.encrypt(req.body.password);
    console.log(password);
    try {
        const queryRegistrar = yield database_1.pool.query("Insert into Persona(ci,nombre,ap_paterno, ap_materno,correo,fono, password) values($1,$2,$3,$4,$5,$6,$7)", [ci, nombre, ap_paterno, ap_materno, correo, fono, password]);
        database_1.pool.query("SELECT * FROM Persona WHERE ci=$1 and nombre= $2 and ap_paterno = $3 and correo= $4 and fono = $5 and  password = $6", [ci, nombre, ap_paterno, correo, fono, password])
            .then((data) => __awaiter(void 0, void 0, void 0, function* () {
            if (data) {
                var datalist = data.rows.map((datos) => {
                    return datos;
                });
                //console.log(datalist[0].id_persona);
                yield database_1.pool.query("Insert into Estudiante(id_persona, institucion, fecha_nacimiento) values ($1, $2,$3)", [datalist[0].id_persona, institucion, fecha_nacimiento])
                    .then((data) => __awaiter(void 0, void 0, void 0, function* () {
                    yield database_1.pool.query("SELECT * FROM Estudiante Where id_persona = $1", [datalist[0].id_persona])
                        .then((data) => __awaiter(void 0, void 0, void 0, function* () {
                        var listData = data.rows.map((dat) => {
                            return dat;
                        });
                        //console.log(listData[0]);
                        yield database_1.pool.query("SELECT * FROM Estudiante p, Persona r WHERE p.id_persona = r.id_persona and p.id_persona =$1 and r.id_persona = $2", [datalist[0].id_persona, listData[0].id_persona])
                            .then((data) => {
                            //console.log(data);
                            res.status(200).json(data.rows);
                        }).catch((err) => {
                            if (err)
                                res.status(400).json({ message: "Internal Server Error pers", err });
                        });
                    })).catch((err) => {
                        res.status(400).json({ message: "Internal Server Error", err });
                    });
                }))
                    .catch((err) => {
                    res.status(400).json({ message: "Internal Server Error", err });
                });
            }
            else
                res.status(400).json({
                    message: "Not Data",
                });
        }))
            .catch((err) => {
            res.status(400).json({ message: "Internal Server Error", err });
        });
    }
    catch (err) {
        res.status(400).json({ message: "Internal Server Error", err });
    }
});
exports.login = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { correo, password } = req.body;
    yield database_1.pool.query("SELECT * FROM Persona Where correo = $1 ", [correo])
        .then((data) => __awaiter(void 0, void 0, void 0, function* () {
        if (data) {
            var listEst = data.rows.map((datae) => { return datae; });
            if (yield bcryptPassword_1.comparePassword(password, listEst[0].password)) {
                const token = jsonwebtoken_1.default.sign({ id: listEst[0].id_persona }, "TOkEN_TESTING");
                res.header("token-auth", token).status(200).json(listEst[0]);
            }
        }
        else
            res.status(404).json("not data");
    }))
        .catch((err) => {
        res.status(400).json({ message: "Internal Server Error", err });
    });
});
exports.profile = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const response = yield database_1.pool.query('SELECT * FROM Persona WHERE id_persona = $1', [req.userId]);
        if (response.rowCount != 0) {
            return res.status(200).json(response.rows);
        }
        else
            return res.status(404).json('Data not found');
    }
    catch (error) {
        return res.status(400).json('internal server error');
    }
});
