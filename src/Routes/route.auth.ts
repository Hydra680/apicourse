import { Router } from "express";
import { login, profile, registraEstudiante } from "../Controller/controller.auth";
import { TokenValidator } from "../Midleware/veryfiToken";

const route = Router();

route.post("/user/registrar", registraEstudiante);
route.post("/user/iniciar", login);
route.get("/user/profile",TokenValidator ,profile);


export default route;